FROM alpine as qemu

RUN if [ -n "aarch64" ]; then \
		wget -O /qemu-aarch64-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-aarch64-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-aarch64-static; \
	fi; \
	chmod a+x /qemu-aarch64-static

FROM registry.gitlab.com/highercomve/flutter-linux-builder:arm64v8 as flutter_builder

COPY --from=qemu /qemu-aarch64-static /usr/bin/

WORKDIR /app
COPY app /app
ENV DEBIAN_FRONTEND=noninteractive

RUN	flutter clean && flutter build linux --release

FROM arm64v8/ubuntu
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app
COPY --from=qemu /qemu-aarch64-static /usr/bin/
COPY --from=flutter_builder /app/build/linux/arm64/release/bundle /app

COPY files /

# RUN apk update && \
# 	apk add weston-clients openrc mesa-dri-gallium libc6-compat mesa-gl gcompat libgcc gtk+3.0

RUN apt update && \
	apt install -y \
	libweston-8-0 \
	libc6 \
	libgcc-10-dev \
	libgl1-mesa-glx \
	libgl1-mesa-dri \
	libgtkd-3-0 \
	libglx-mesa0 \
	gcc 

RUN chmod +x /usr/local/bin/example_flutter_app.sh && \
	mv /app /usr/lib/example_app && \
	ln -s /usr/lib/example_app/example_app /usr/bin/example_app

# RUN mkdir -p /etc/runlevels/default/ && \
# 	ln -fs /etc/init.d/pv-example-flutter /etc/runlevels/default/pv-example-flutter

RUN mkdir -p /etc/systemd/system/multi-user.target.wants/ || true \
	&& ln -s /etc/systemd/system/pv-example-flutter.service /etc/systemd/system/multi-user.target.wants/ 

CMD [ "/usr/local/bin/example_flutter_app.sh" ]